# Copyright 2019 Karsten Roth and Biagio Brattoli
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================


############################ LIBRARIES ######################################
import torch.nn as nn

from nets.resnet import ResNet50, ResNet101
from nets.se_resnet import se_resnet50, se_resnext50_32x4d, se_resnext101_32x4d


"""============================================================="""
def initialize_weights(model):
    """
    Function to initialize network weights.
    NOTE: NOT USED IN MAIN SCRIPT.

    Args:
        model: PyTorch Network
    Returns:
        Nothing!
    """
    for idx,module in enumerate(model.modules()):
        if isinstance(module, nn.Conv2d):
            nn.init.kaiming_normal_(module.weight, mode='fan_out', nonlinearity='relu')
        elif isinstance(module, nn.BatchNorm2d):
            nn.init.constant_(module.weight, 1)
            nn.init.constant_(module.bias, 0)
        elif isinstance(module, nn.Linear):
            module.weight.data.normal_(0,0.01)
            module.bias.data.zero_()



"""=================================================================================================================================="""
### ATTRIBUTE CHANGE HELPER
def rename_attr(model, attr, name):
    """
    Rename attribute in a class. Simply helper function.

    Args:
        model:  General Class for which attributes should be renamed.
        attr:   str, Name of target attribute.
        name:   str, New attribute name.
    """
    setattr(model, name, getattr(model, attr))
    delattr(model, attr)


"""=================================================================================================================================="""
### NETWORK SELECTION FUNCTION
def networkselect(args):
    """
    Selection function for available networks.

    Args:
        opt: argparse.Namespace, contains all training-specific training parameters.
    Returns:
        Network of choice
    """
    if args.arch == 'resnet50':
        network = ResNet50(args)
    elif args.arch == 'resnet101':
        network = ResNet101(args)
    elif args.arch == 'se_resnet50':
        network = se_resnet50(regional=args.regional, pooling=args.pool, whitening=args.whitening, pretrained=args.pretrained, bs=args.bs,
                              input_size=args.input_size, embed_dim=args.embed_dim)
    elif args.arch == 'se_resnext50':
        network = se_resnext50_32x4d(regional=args.regional, gds=args.gds, whitening=args.whitening, pretrained=args.pretrained, bs=args.bs,
                                     input_size=args.input_size, embed_dim=args.embed_dim)
    elif args.arch == 'se_resnext101':
        network = se_resnext101_32x4d(regional=args.regional, pooling=args.pool, whitening=args.whitening, pretrained=args.pretrained, bs=args.bs,
                                      input_size=args.input_size, embed_dim=args.embed_dim)
    else:
        raise Exception('Network {} not available!'.format(args.arch))
    return network
