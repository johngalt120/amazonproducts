import warnings
warnings.filterwarnings("ignore")
import os, sys, numpy as np, argparse, time
os.chdir(os.path.dirname(os.path.realpath(__file__)))

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt


from tqdm import tqdm
from functools import partial
import torch
from torch.nn.parallel import DataParallel as DP
import auxiliaries as aux
import datasets as data
import netlib
import losses as losses
import evaluate as eval
import torch.multiprocessing as mp
import metrics


def parse_args(args):
    parser = argparse.ArgumentParser()

    parser.add_argument('--master', metavar='address:port', type=str, help='Adress and port of the master worker', default='127.0.0.1:29500')
    parser.add_argument('--dataset',      default='Amazon',   type=str, help='Dataset to use.')

    ### General Training Parameters
    parser.add_argument('--lr',                default=0.00001,  type=float, help='Learning Rate for network parameters.')
    parser.add_argument('--fc_lr_mul',         default=0,        type=float, help='OPTIONAL: Multiply the embedding layer learning rate by this value. If set to 0, the embedding layer shares the same learning rate.')
    parser.add_argument('--n_epochs',          default=192,       type=int,   help='Number of training epochs.')
    parser.add_argument('--kernels',           default=8,        type=int,   help='Number of workers for pytorch dataloader.')
    parser.add_argument('--bs',                default=112 ,     type=int,   help='Mini-Batchsize to use.')
    parser.add_argument('--samples_per_class', default=4,        type=int,   help='Number of samples in one class drawn before choosing the next class. Set to >1 for losses other than ProxyNCA.')
    parser.add_argument('--scheduler',         default='step',   type=str,   help='Type of learning rate scheduling. Currently: step & exp.')
    parser.add_argument('--gamma',             default=0.3,      type=float, help='Learning rate reduction after tau epochs.')
    parser.add_argument('--decay',             default=0.0004,   type=float, help='Weight decay for optimizer.')
    parser.add_argument('--tau',               default=[72, 108, 144, 162],nargs='+',type=int,help='Stepsize(s) before reducing learning rate.')
    parser.add_argument('--input_size', default=224, type=int, help='ims size')

    ##### Loss-specific Settings
    parser.add_argument('--loss',         default='marginloss', type=str,   help='loss options: marginloss, triplet, npair, proxynca')
    parser.add_argument('--sampling',     default='distance',   type=str,   help='For triplet-based losses: Modes of Sampling: random, semihard, distance.')
    ### MarginLoss
    parser.add_argument('--margin',       default=0.1,          type=float, help='TRIPLET/MARGIN: Margin for Triplet-based Losses')
    parser.add_argument('--beta_lr',      default=0.0005,       type=float, help='MARGIN: Learning Rate for class margin parameters in MarginLoss')
    parser.add_argument('--beta',         default=1.2,          type=float, help='MARGIN: Initial Class Margin Parameter in Margin Loss')
    parser.add_argument('--nu',           default=0,            type=float, help='MARGIN: Regularisation value on betas in Margin Loss.')
    parser.add_argument('--beta_constant',                      action='store_true', help='MARGIN: Use constant, un-trained beta.')
    ### ProxyNCA
    parser.add_argument('--proxy_lr',     default=0.00001,     type=float, help='PROXYNCA: Learning Rate for Proxies in ProxyNCALoss.')
    ### NPair L2 Penalty
    parser.add_argument('--l2npair',      default=0.02,        type=float, help='NPAIR: Penalty-value for non-normalized N-PAIR embeddings.')

    ##### Evaluation Settings
    parser.add_argument('--k_vals',       nargs='+', default=[1,3,10,50], type=int, help='Recall @ Values.')

    ##### Network parameters
    parser.add_argument('--device', default='cuda', type=str)
    parser.add_argument('--embed_dim',    default=128,         type=int,   help='Embedding dimensionality of the network. Note: in literature, dim=128 is used for ResNet50 and dim=512 for GoogLeNet.')
    parser.add_argument('--arch',         default='se_resnet50',  type=str,   help='Network backend choice: resnet50, googlenet, se_resnet50, se_resnet101')

    parser.add_argument('--not_pretrained', default=True, help='If added, the network will be trained WITHOUT ImageNet-pretrained weights.')
    parser.add_argument('--grad_measure',                      action='store_true', help='If added, gradients passed from embedding layer to the last conv-layer are stored in each iteration.')
    parser.add_argument('--dist_measure',                      action='store_true', help='If added, the ratio between intra- and interclass distances is stored after each epoch.')
    parser.add_argument('--pretrained', type=str, help='dataset name for pretrained model')

    parser.add_argument('--savename',     default='',          type=str,   help='Save folder name if any special information is to be included.')

    parser.add_argument('--metric', type=str)
    parser.add_argument('--source_path',  default=os.getcwd()+'/Datasets',         type=str, help='Path to training data.')
    parser.add_argument('--save_path',    default=os.getcwd()+'/Training_Results', type=str, help='Where to save everything.')
    parser.add_argument('--checkpoint_path', type=str, help='path to trained model weights', default='/app/pretrained/checkpoint.pth.tar')
    parser.add_argument('--command', default='inference', type=str)
    parser.add_argument('--categories_num', default=10, type=int, help='num categories for inference mode')
    parser.add_argument('--imgs_per_cat', default=2, type=int, help='num images per category for inference mode')
    parser.add_argument('--n_closest', default=10, type=int, help='number of closest images to target')

    parser.add_argument('--distance', default='euclidean', type=str, help='Distance measurement between embeddings')
    parser.add_argument('--checkpoint_path', type=str, help='Path to pretrained model')
    parser.add_argument('--categories_num', default=30, type=int, help='num categories for inference mode')
    parser.add_argument('--imgs_per_cat', default=2, type=int, help='num images per category for inference mode')
    parser.add_argument('--n_closest', default=10, type=int, help='number of closest images to target')

    parser.add_argument('--regional', default=True, help='train model with regional pooling using fixed grid')
    parser.add_argument('--whitening', default=True, help='train model with learnable whitening (linear layer) after the pooling')
    parser.add_argument('--gds', '-p', metavar='POOL', nargs='+', default='gem', choices=['mac', 'spoc', 'gem', 'gemmp'],
                        help='pooling options: ' +
                             ' | '.join(['mac', 'spoc', 'gem', 'gemmp']) +
                             ' (default: gem)')
    return parser.parse_args()


def main(args):
    args.source_path += '/' + args.dataset
    args.save_path += '/' + args.dataset

    if args.dataset == 'Amazon':
        args.k_vals = [1, 3, 10, 50]

    if args.loss == 'proxynca':
        args.samples_per_class = 1
    else:
        assert not args.bs % args.samples_per_class, 'Batchsize needs to fit number of samples per class for distance sampling and margin/triplet loss!'

    if args.loss == 'npair' or args.loss == 'proxynca':
        args.sampling = 'None'

    if args.checkpoint_path:
        args.pretrained = None
        args.not_pretrained = True

    model = netlib.networkselect(args)
    if model: model.share_memory()

    gpus_num = torch.cuda.device_count()

    worker(args, model, gpus_num)


def initialize_network(model, pretrained_state_dict):
    state_dict = model.state_dict()

    pretrained_dict = {k: v for k, v in pretrained_state_dict.items() if k in state_dict}
    # 2. overwrite entries in the existing state dict
    state_dict.update(pretrained_dict)
    # 3. load the new state dict
    model.load_state_dict(state_dict)
    return model


def worker(args, model, gpus_num):
    if gpus_num > 1:
        model = DP(model, device_ids=range(torch.cuda.device_count())).cuda()
        args.bs = args.bs * gpus_num
    else:
        model = DP(model).cuda()

    dataloader = data.give_dataloaders(args.dataset, args)
    args.num_classes = len(dataloader['training'].dataset.avail_classes)

    metrics_to_log = aux.metrics_to_examine(args.dataset, args.k_vals)
    logger = aux.LOGGER(args, metrics_to_log, name='Base', start_new=True)

    if args.checkpoint_path:
        state_dict = torch.load(args.checkpoint_path)['state_dict']
        model = initialize_network(model, state_dict)
        print(f"loading checkpoint from : {args.checkpoint_path}")

    if args.command == 'train':
        for name, param in model.named_parameters():
            if param.requires_grad:
                if name.startswith('module.layer'):
                    param.requires_grad = False

    dataloader = data.give_dataloaders(args.dataset, args)
    args.num_classes = len(dataloader['training'].dataset.avail_classes)

    metrics_to_log = aux.metrics_to_examine(args.dataset, args.k_vals)
    logger = aux.LOGGER(args, metrics_to_log, name='Base', start_new=True)

    if args.command == 'train':
        train(model=model, dataloader=dataloader, logger=logger, args=args)
    elif args.command == 'inference':
        model.eval()
        eval.find_closest_images(model=model, dataloader=dataloader['evaluation'], categories_num=args.categories_num,
                                 args=args)
    elif args.command == 'clean_ds':
        state_dict = torch.load(args.checkpoint_path)['state_dict']
        model = initialize_network(model, state_dict)
        model.eval()

        tasks = get_files(args.source_path+'/images')

        pool = mp.Pool(8)
        func = partial(eval.clean_ds, model, args.input_size, args.device, use_category=True)
        pool.map(func, tasks)
        pool.close()
        pool.join()


def train(model, dataloader, logger, args, is_master=True):
    if 'fc_lr_mul' in vars(args).keys() and args.fc_lr_mul != 0:
        all_but_fc_params = list(filter(lambda x: 'last_linear' not in x[0], model.named_parameters()))
        fc_params = model.model.last_linear.parameters()
        to_optim = [{'params': all_but_fc_params, 'lr': args.lr, 'weight_decay': args.decay},
                    {'params': fc_params, 'lr': args.lr * args.fc_lr_mul, 'weight_decay': args.decay}]
    else:
        to_optim = [{'params': model.parameters(), 'lr': args.lr, 'weight_decay': args.decay}]

    try:
        aux.save_graph(args, model)
    except:
        print('Cannot generate graph!')

    grad_measure, dist_measure = None, None
    torch.backends.cudnn.benchmark = True

    if args.grad_measure:
        grad_measure = eval.GradientMeasure(args, name='baseline')

    if args.dist_measure:
        dist_measure = eval.DistanceMeasure(dataloader['evaluation'], args, name='Train', update_epochs=1)

    criterion, to_optim = losses.loss_select(args.loss, args, to_optim)

    if torch.cuda.is_available():
        criterion = criterion.cuda()

    if args.metric == 'ArcFace':
        metric = metrics.ArcFace(args.embed_dim, args.num_classes, device='cuda', m=0.3)
    elif args.metric == 'CosFace':
        metric = metrics.CosFace(args.embed_dim, args.num_classes, device='cuda', m=0.3)
    elif args.metric == 'AmSoftmax':
        metric = metrics.Am_softmax(args.embed_dim, args.num_classes, device='cuda', m=0.3)
    else:
        metric = None

    if metric is not None:
        to_optim.append({'params': metric.parameters()})

    optimizer = torch.optim.Adam(to_optim)

    if args.scheduler == 'exp':
        scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma=args.gamma)
    elif args.scheduler == 'step':
        scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=args.tau, gamma=args.gamma)
    elif args.scheduler == 'none':
        print('No scheduling used!')
    else:
        raise Exception('No scheduling option for input: {}'.format(args.scheduler))

    for epoch in range(args.n_epochs):
        if args.scheduler != 'none' and is_master: print(
            'Running with learning rates {}...'.format(' | '.join('{}'.format(x) for x in scheduler.get_lr())))
        model.train()

        train_one_epoch(dataloader['training'], model, optimizer, criterion, args, epoch, logger, grad_measure, metric)

        if args.num_classes <= 200:
            model.eval()

            eval_params = {'dataloader': dataloader['testing'], 'model': model, 'args': args, 'epoch': epoch}
            eval.evaluate(args.dataset, logger, save=True, **eval_params)

            logger.update_info_plot()

        else:
            print("Too large dataset. Use no faiss for evaluation!")
            aux.set_checkpoint(model, args, logger.progress_saver, logger.prop.save_path + '/checkpoint.pth.tar')
        eval_params = {'dataloader': dataloader['testing'], 'model': model, 'args': args, 'epoch': epoch}

        eval.evaluate(args.dataset, logger, save=True, **eval_params)

        logger.update_info_plot()

        if args.dist_measure:
            dist_measure.measure(model, epoch)

        if args.scheduler != 'none':
            scheduler.step()


def get_files(ds_path):
    tasks = [full_paths for full_paths in [[os.path.join(ds_path, image_cat, item_id, review, fname) for fname in
            os.listdir(os.path.join(ds_path, image_cat, item_id, review))] for image_cat in
            os.listdir(ds_path) for item_id in
            os.listdir(os.path.join(ds_path, image_cat)) for review in
            os.listdir(os.path.join(ds_path, image_cat, item_id))] if len(full_paths) >= 4]

    return tasks


def train_one_epoch(train_dataloader, model, optimizer, criterion, args, epoch, LOG, grad_measure, metric):
    loss_collect = []

    start = time.time()

    data_iterator = tqdm(train_dataloader, desc='Epoch {} Training...'.format(epoch))
    for i, (class_labels, data) in enumerate(data_iterator):
        if torch.cuda.is_available():
            data = data.cuda()
        features = model(data)
        if metric is not None:
            features = metric(features, class_labels)
        loss = criterion(features, class_labels)

        optimizer.zero_grad()

        loss.backward()

        if grad_measure:
            args.include(model.model.last_linear)

        optimizer.step()

        loss_collect.append(loss.item())
        if i == len(train_dataloader)-1:
            data_iterator.set_description('Epoch (Train) {0}: Mean Loss [{1:.4f}]'.format(epoch, np.mean(loss_collect)))

    LOG.log('train', LOG.metrics_to_log['train'], [epoch, np.round(time.time()-start, 4), np.mean(loss_collect)])

    if args.grad_measure:
        grad_measure.dump(epoch)


if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    if args.command == 'clean_ds':
        mp.set_start_method('spawn', force=True)
    main(args)
