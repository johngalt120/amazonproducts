import pretrainedmodels
import torch.nn as nn

from nets.resnet_helpers import *
from nets.pooling import MAC, SPoC, GeM, RMAC, GeMmp, Rpool


POOLING = {
    'mac'   : MAC,
    'spoc'  : SPoC,
    'gem'   : GeM,
    'gemmp' : GeMmp,
    'rmac'  : RMAC,
}


def se_resnet50(regional, whitening, pooling, bs, input_size, embed_dim, num_classes=1000, pretrained=None):
    pool = POOLING[pooling]()

    if regional:
        rpool = pool
        rwhiten = nn.Linear(2048, embed_dim, bias=True)

        pool = Rpool(rpool, rwhiten)

    if whitening:
        whiten = nn.Linear(embed_dim, embed_dim, bias=True)
    else:
        whiten = None

    model = SENet(SEResNetBottleneck, [3, 4, 6, 3], groups=1, reduction=16,
                  inplanes=64, input_3x3=False, embed_dim=embed_dim,
                  downsample_kernel_size=1, bs=bs, downsample_padding=0,
                  input_size=input_size, pool=pool, whiten=whiten)
    if pretrained:
        settings = pretrained_settings['se_resnet50'][pretrained]
        initialize_pretrained_model(model, num_classes, settings)
    return model


def se_resnext50_32x4d(regional, whitening, bs, input_size, embed_dim, gds, num_classes=1000, pretrained=None):
    pooling_layers = {descriptor: POOLING[descriptor]() for descriptor in gds}

    layers = dict()
    for descriptor in pooling_layers:
        if regional:
            rpool = pooling_layers[descriptor]
            rwhiten = nn.Linear(2048, 2048, bias=True)

            pool = Rpool(rpool, rwhiten)
            layers[descriptor] = pool

    if whitening:
        whiten = nn.Linear(2048 * len(gds), embed_dim, bias=True)
    else:
        whiten = None

    layers = nn.ModuleDict(layers)

    model = SENet(SEResNeXtBottleneck, [3, 4, 6, 3], groups=32, reduction=16,
                  inplanes=64, input_3x3=False, bs=bs, embed_dim=embed_dim,
                  downsample_kernel_size=1, downsample_padding=0,
                  input_size=input_size, pool=layers, whiten=whiten)
    if pretrained:
        settings = pretrained_settings['se_resnext50_32x4d'][pretrained]
        initialize_pretrained_model(model, num_classes, settings)
    return model


def se_resnext101_32x4d(regional, whitening, pooling, bs, input_size, embed_dim, num_classes=1000, pretrained=None):
    pool = POOLING[pooling]()

    if regional:
        rpool = pool
        rwhiten = nn.Linear(2048, embed_dim, bias=True)

        pool = Rpool(rpool, rwhiten)

    if whitening:
        whiten = nn.Linear(embed_dim, embed_dim, bias=True)
    else:
        whiten = None

    model = SENet(SEResNeXtBottleneck, [3, 4, 23, 3], groups=32, reduction=16,
                  inplanes=64, input_3x3=False, bs=bs, embed_dim=embed_dim,
                  downsample_kernel_size=1, downsample_padding=0,
                  input_size=input_size, pool=pool, whiten=whiten)
    if pretrained:
        settings = pretrained_settings['se_resnext101_32x4d'][pretrained]
        initialize_pretrained_model(model, num_classes, settings)
    return model
