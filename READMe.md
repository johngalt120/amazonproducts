https://github.com/Confusezius/Deep-Metric-Learning-Baselines  
https://arxiv.org/pdf/1903.10663.pdf  

**[ЗАВИСИМОСТИ]**  
docker + Docker-Compose + nvidia-docker  
Linux or macOS

Для сборки образа: docker-compose build --force-rm с правами sudo 

**[ПОДГОТОВКА ДАННЫХ]**  
AmazonProducts dataset имеет следующую структуру: AmazonProducts -> images -> ImageCategory -> ProductId -> ReviewCategory -> *.jpg  
Если датасет предварительно очищен, и категории перемешаны, то ReviewCategory отсутствует (удалить ее из giveAmazon в datasets.py)

**[ОБУЧЕНИЕ]**  
Параметры запуска задаются через команду в docker-compose.yml
command: python3 /app/Standard_Training.py --bs 96 (размер батча)  
                                           --arch se_resnext50  
                                           --lr 0.0003  
                                           --decay 0.01
                                           --embed_dim 768 (размерность выходного дескриптора, 512 выглядит недостаточной)    
                                           --gamma 0.1  
                                           --sampling distance (https://arxiv.org/pdf/1706.07567.pdf)  
                                           --gds gem spoc mac (комбинация из глобальных дескрипторов из которой получается выходной дескриптор)   
                                           --input_size 280 (обучение заточенно под резайс 280 и случайный кроп 252)  
                                           --distance euclidean (метрика близости)   
                                           --command train (train/inference/clean_ds)    
                                           --pretrained imagenet  
                                           --loss marginloss (https://arxiv.org/pdf/1706.07567.pdf)   
                                           --checkpoint_path Training_Results/Amazon/category_classify_best/checkpoint.pth.tar  
                                           --metric ArcFace (https://arxiv.org/pdf/1801.07698.pdf)   

### Процесс обучения: 
**Feedforward сети отдает 3 вектора (полносвязный слой) перед классифицией с обучаемым параметром,
вектора конкатенируются, и результат подаётся на еще один fc-слой размерностью embed_dim, полученный  
вектор либо используется, как есть, в triplet loss, либо подается в metric (метрики из face recognition
раздвигают вектора на гиперсфере), а потом в binary/focal loss.**

**Пайплайн: Сеть, обученная на imagenet -> Заморозка всех слоев кроме 4-го -> обучение на классификацию 
категории -> Заморозка всех слоев и использование классификатора, как претрейна -> обучение получения
дескриптора.**


**[ИНФЕРЕНС]**  
В режиме инференса задается categories_num (сколько объектов) и n_closest (число ближайших соседей для них) 
в command, результат сохраняется в /app/res.png.

**[ОЧИСТКА ДАТАСЕТА]**  
В режиме очистки отбираются самые похожие изображения в категориях main_images/review_images, и результат
сохраняется в одной директории (productId)

**[ОТЛИЧИЯ ОТ ОРИГИНАЛЬНОЙ РЕПЫ]**  
- Добавлен clean_ds/inference mode
- Добавлены глобальные дескрипторы gem / spoc / mac
- Добавлены метрики ArcFace / CosFace / SphereFace
- Добавлены SeResNet, SeResNext